let emojiHandButton = document.querySelector("#emoji-hand-button");
let emojiThumbButton = document.querySelector("#emoji-thumb-button");
let emojiHeartButton = document.querySelector("#emoji-heart-button");
let emojiApplauseButton = document.querySelector("#emoji-applause-button");
let emojiLaughButton = document.querySelector("#emoji-laugh-button");

emojiHandButton.addEventListener("click", () => {
  const videoContainer = document.querySelector("#video-container");
  const emojiChild = document.getElementById("emoji");
  let className = "emoji-hand"

  if(emojiChild != null) {
    // Caso: Hay Emoji en el Contenedor
    if(emojiChild.classList.contains(className)) {
      // Caso: Click en el mismo emoji
      emojiChild.remove();
    }
    else {
      // Caso: Click en otro emoji
      emojiChild.remove();

      let emoji = createEmojiHand();
      videoContainer.appendChild(emoji); 
    }
  }
  else {
    // Caso: No hay Emoji en Contenedor
    let emoji = createEmojiHand();
    videoContainer.appendChild(emoji); 
  }
});

emojiThumbButton.addEventListener("click", () => {
  const videoContainer = document.querySelector("#video-container");
  const emojiChild = document.getElementById("emoji");
  let className = "emoji-thumb-up";

  if (emojiChild != null) {
    if (emojiChild.classList.contains(className)) {
      emojiChild.remove();
    }
    else {
      emojiChild.remove();

      let emoji = createEmojiLike();
      videoContainer.appendChild(emoji); 
    }
  }
  else {
    let emoji = createEmojiLike();
    videoContainer.appendChild(emoji); 
  }
});

emojiHeartButton.addEventListener("click", () => {
  const videoContainer = document.querySelector("#video-container");
  const emojiChild = document.getElementById("emoji");
  let className = "emoji-heart";

  if (emojiChild != null) {
    if (emojiChild.classList.contains(className)) {
      emojiChild.remove();
    }
    else {
      emojiChild.remove();

      let emoji = createEmojiHeart();
      videoContainer.appendChild(emoji);
    }
  }
  else {
    let emoji = createEmojiHeart();
    videoContainer.appendChild(emoji);
  }
});

/* Aplauso */
emojiApplauseButton.addEventListener("click", () => {
  const videoContainer = document.querySelector("#video-container");
  const emojiChild = document.getElementById("emoji");
  let className = "emoji-applause";

  if (emojiChild != null) {
    if (emojiChild.classList.contains(className)) {
      emojiChild.remove();
    }
    else {
      emojiChild.remove();

      let emoji = createEmojiApplause();
      videoContainer.appendChild(emoji);
    }
  }
  else {
    let emoji = createEmojiApplause();
    videoContainer.appendChild(emoji);
  }
});

/* Creates Emoji Laugh Animation */
emojiLaughButton.addEventListener("click", () => {
  const videoContainer = document.querySelector("#video-container");
  const emojiChild = document.getElementById("emoji");
  let className = "emoji-haha"

  if (emojiChild != null) {
    if (emojiChild.classList.contains(className)) {
      emojiChild.remove();
    }
    else {
      emojiChild.remove();

      let emoji = createEmojiLaugh();
      videoContainer.appendChild(emoji);
    }
  }
  else {
    let emoji = createEmojiLaugh();
    videoContainer.appendChild(emoji);
  }
})

const createEmojiHand = () => {
  const emoji = document.createElement("div");
  emoji.innerHTML = "👋";
  emoji.setAttribute("id", "emoji")
  emoji.classList.add("emoji-reaction", "emoji-hand", "show");
  return emoji;
}

const createEmojiLike = () => {
  const emoji = document.createElement("div");
  emoji.innerHTML = "👍";
  emoji.setAttribute("id", "emoji");
  emoji.classList.add("emoji-reaction", "emoji-thumb-up", "show");
  return emoji;
}

const createEmojiHeart = () => {
  const emoji = document.createElement("div");
  emoji.innerHTML = "❤️";
  emoji.setAttribute("id", "emoji");
  emoji.classList.add("emoji-reaction", "emoji-heart", "show");
  return emoji;
}

const createEmojiApplause = () => {
  const emojiContainer = document.createElement("div");
  emojiContainer.setAttribute("id", "emoji");
  emojiContainer.classList.add("emoji-reaction", "emoji-applause", "show");

  const emojiLeftHand = document.createElement("div");
  emojiLeftHand.innerHTML = "🤚";
  emojiLeftHand.classList.add("emoji-left-hand");
  relative
  const emojiRightHand = document.createElement("div");
  emojiRightHand.innerHTML = "🤚";
  emojiRightHand.classList.add("emoji-right-hand");

  emojiContainer.appendChild(emojiLeftHand);
  emojiContainer.appendChild(emojiRightHand);

  return emojiContainer;
}

const createEmojiLaugh = () => {
  const emojiContainer = document.createElement("div");
  emojiContainer.setAttribute("id", "emoji");
  emojiContainer.classList.add("emoji-reaction", "emoji", "emoji-haha", "show");

  const emojiFace = document.createElement("div");
  emojiFace.classList.add("emoji-face");

  const emojiEyes = document.createElement("div");
  emojiEyes.classList.add("emoji-eyes");

  const emojiMouth = document.createElement("div");
  emojiMouth.classList.add("emoji-mouth");

  const emojiTongue = document.createElement("div");
  emojiTongue.classList.add("emoji-tongue");

  emojiMouth.appendChild(emojiTongue);
  emojiFace.appendChild(emojiEyes);
  emojiFace.appendChild(emojiMouth);
  emojiContainer.appendChild(emojiFace);

  return emojiContainer;
}