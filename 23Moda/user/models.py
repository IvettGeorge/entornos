from typing import AbstractSet
from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class User(AbstractUser):
    code=models.CharField(max_length=150,verbose_name='Codigo')
    verified=models.IntegerField(default=0)

    def save(self, *args, **kwargs):
        if self.pk is None:
            self.set_password(self.password)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.email
