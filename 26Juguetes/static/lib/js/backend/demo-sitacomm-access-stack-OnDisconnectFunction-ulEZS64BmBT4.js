// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10', region: process.env.AWS_REGION });
const { TABLE_NAME } = process.env;
const  TABLE_DETAILS='demo_sitacomm_access_details';


exports.onDisconnect = async event => {
    
  
  const deleteParams = {
    TableName: TABLE_NAME,
    Key: {
      connectionId: event.requestContext.connectionId
    }
  };

  try {
    await ddb.delete(deleteParams).promise();
  } catch (err) {
    return { statusCode: 500, body: 'Failed to disconnect: ' + JSON.stringify(err) };
  }

		
const params = {
			TableName: TABLE_DETAILS,
             Key: {
				idDetail:""+event.requestContext.connectionId
			},
			UpdateExpression: "set activo = :a, getoutat=:o",
			ExpressionAttributeValues:{
				":a":!true,
				":o": ""+Date.now()
			},
			
			ReturnValues:"ALL_OLD"//[ALL_NEW, UPDATED_OLD, ALL_OLD, NONE, UPDATED_NEW]
        };
		
	
	try {
        await ddb.update(params).promise();
    
  } catch (err) {
    console.log(err);
    return { statusCode: 500, body: 'Failed to disconnect: ' + JSON.stringify(err) };
  }

		
  return { statusCode: 200, body: 'Disconnected.' };
};
