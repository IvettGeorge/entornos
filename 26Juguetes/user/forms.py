from django.forms import *

from user.models import User
from core.models import Event, Event_User

class UserForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['autofocus'] = True

    class Meta:
        model = User
        fields = '__all__'
        widgets = {
            'username': TextInput(
                attrs={
                    'placeholder': 'Usuario',
                }
            ),
            'first_name': TextInput(
                attrs={
                    'placeholder': 'Nombre(s)',
                }
            ),
            'last_name': TextInput(
                attrs={
                    'placeholder': 'Apellido',
                }
            ),
            'email': TextInput(
                attrs={
                    'placeholder': 'Email',
                }
            ),
            'code': TextInput(
                attrs={
                    'placeholder': 'Código',
                }
            ),
            'verified': TextInput(
                attrs={
                    'placeholder': 'Código',
                    'value':'0',
                }
            ),
            'password':PasswordInput(
                attrs={
                    'placeholder': 'Password',
                }
            ),
        }
        exclude=['last_login','is_superer','is_staff','is_active','date_joined']

        def save(self, commit=True):
            data={}
            form=super()
            try: 
                if form.is_valid():
                    form.save()
                else:
                    data['error']=form.errors
            except Exception as e:
                data['error']=str(e)
            return data



class EventForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        #self.fields['username'].widget.attrs['autofocus'] = True

    class Meta:
        model = Event
        fields = '__all__'
        widgets = {
            'name': TextInput(
                attrs={
                    'placeholder': 'Nombre Evento',
                }
            ),
            'idStar': TextInput(
                attrs={
                    'placeholder': 'ID evento',
                }
            ), 'code': TextInput(
                attrs={
                    'placeholder': 'Codigo',
                }
            ),
          
        }
        exclude=['date_created','date_update', 'start', 'end']

        def save(self, commit=True):
            data={}
            form=super()
            try: 
                if form.is_valid():
                    form.save()
                else:
                    data['error']=form.errors
            except Exception as e:
                data['error']=str(e)
            return data




class EventUserForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        #self.fields['username'].widget.attrs['autofocus'] = True

    class Meta:
        model = Event_User
        fields = '__all__'
        widgets = {
            'id_eventSS': TextInput(
                attrs={
                    'placeholder': 'Id evento tickets',
                }
            ),
            'id_eventDS': TextInput(
                attrs={
                    'placeholder': 'ID evento Demo',
                }
            ), 'code': TextInput(
                attrs={
                    'placeholder': 'Codigo',
                }
            ),
          
          'idUserSS': TextInput(
                attrs={
                    'placeholder': 'ID del usuario SS',
                }
            ),
            'idUserDS': TextInput(
                attrs={
                    'placeholder': 'ID del usuario Demo',
                }
            ),    
            'mail': TextInput(
                attrs={
                    'placeholder': 'mail usuario SS',
                }
            ),           
        }
        exclude=['date_created','date_update']

        def save(self, commit=True):
            data={}
            form=super()
            try: 
                if form.is_valid():
                    form.save()
                else:
                    data['error']=form.errors
            except Exception as e:
                data['error']=str(e)
            return data