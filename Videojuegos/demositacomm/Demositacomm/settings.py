"""
Django settings for Demositacomm project.

Generated by 'django-admin startproject' using Django 3.0.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.0/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '%x1u$=@i=qrv=9hx5)qh=nlp$bqsrqudr5=!84oc@)g8%*r_va'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['127.0.0.1',
'demo-env.eba-pbmd3icr.us-east-1.elasticbeanstalk.com',
'demo.starticket.com.mx',
'sports.starticket.com.mx',
'fitness.starticket.com.mx',
'videos.starticket.com.mx',
'demo.multiboleto.com',
'demo.multiboleto.com',
'videojuegos.multiboleto.com',
'videojuegos-env.eba-idixbd9c.us-east-1.elasticbeanstalk.com',
]

AUTHENTICATION_BACKENDS=[
#'django.contrib.auth.backends.ModelBackend',
#'sesame.backends.ModelBackend',
#'user.authentication.EmailBackend',
#'user.authentication.EmailEventUserBackend',
'user.authentication.EmailEventUserMessagesBackend'
]

# Número de segundos de inactividad antes de que un usuario se marca fuera de línea
USER_ONLINE_TIMEOUT = 360
 
# Número de segundos que vamos a hacer seguimiento de los usuarios inactivos, para antes de su 
# visto por última vez. Se elimina de la memoria caché
USER_LASTSEEN_TIMEOUT = 60 * 60 * 24 * 7

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

#Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'widget_tweaks',
    'corsheaders',

    #apps
    'user',
    'core',
    'login',
    'analytics',

]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',    
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'sesame.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'sesame.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]


ROOT_URLCONF = 'Demositacomm.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR,'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'Demositacomm.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', 
        'NAME': 'demositacomm',
        'USER': 'admin',
        'PASSWORD': 'SitacommDEMO',
        'HOST': 'demositacomm.cefsfrtoeeqr.us-east-1.rds.amazonaws.com',   # Or an IP Address that your DB is hosted on
        'PORT': '3306',
    }
}

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = 'es-mx'

TIME_ZONE = 'America/Mexico_City'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATIC_URL = '/static/'

#STATICFILES_DIRS = [
#    "static",
#]

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

AUTH_USER_MODEL='user.User'

LOGIN_REDIRECT_URL= "/home"

LOGOUT_REDIRECT_URL="/login"

DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

AWS_S3_ACCESS_KEY_ID='AKIA3ZMW5VVOMWJBD5QZ'

AWS_S3_SECRET_ACCESS_KEY='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'

AWS_STORAGE_BUCKET_NAME='demositacomm'

AWS_QUERYSTRING_AUTH=False

CODE_EVENT='61f3248d89400'
# If this is used then `CORS_ALLOWED_ORIGINS` will not have any effect

CORS_ALLOW_ALL_ORIGINS = True 
CORS_ALLOW_CREDENTIALS = True

