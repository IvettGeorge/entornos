"""Demositacomm URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from user.views import *

from core.views import *
from user.views import *
from login.views import *
from user.authentication import *
from analytics.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
#    path('demo/', include('core.urls')),
    path('register/',UserCreateView.as_view(), name='registro'),
    path('registerpost/',UserApiCreateview.as_view(), name='registroAPI'),
    path('login2',LoginFormView.as_view(), name="login2"),
    path('logout',LogoutRedirectView.as_view(), name="logout"),
    path('login',login_view, name="login"),

    path('',firtsTemplate,name="index"),
    path('agenda/',agendaListView.as_view(), name="Agenda"),
    path('home/', inicioListView.as_view(),name="home"),
    path('createQuestion/',QuestionCreateView.as_view(), name="createQuestion"),
    path('createSurvey/',SurveyCreateView.as_view(), name="createSurvey"),
    path('login/<str:token>',loginSesame, name="loginSesameLog"),
    path('createSpeaker/',SpeakerCreateView.as_view(), name="createSpeaker"),
    path('createQuestionExt/',QuestionExter.as_view(), name="createQuestionExt"),
    path('speakers/',speakerListView.as_view(), name="Speakers"),
    path('questions/',questionListView.as_view(), name="Questions"),
    path('breakouts/',breakouts, name="Breakouts"),
    path('meeting/',meet1ListView.as_view(), name="Meet"),
    path('meeting/meet/<str:name>/<int:mn>/<str:email>/<str:pwd>/<int:role>/<slug:lang>/<str:signature>/<int:china>/<str:apiKey>',meet2),
    path('testLogin/',loginListView.as_view(), name="testLogin"),

    path('createEvent/',EventApiCreateview.as_view(), name='createEvent'),
    path('createAccessUserEvent/',EventUserApiCreateview.as_view(), name='createAccessUserEvent'),
    path('updateUserEvent/',UserApiUpdateView.as_view(), name='updateUserEvent'),
    path('updateEvent/',EventApiUpdateview.as_view(), name='updateEvent'),
    path('request-login/', include('login.urls')),
    path('auto-login',auto_login ,name="auto-login"),
    path('analytics',include('analytics.urls')),
    path('maps',maps ,name="maps"),
    path('maps2',maps2 ,name="maps2"),
    path('getAnalytics',dictCountry ,name="getCountry"),
    path('getGraph',graphData ,name="getGraph"),
    path('getGraphTech',graphDataTech ,name="getGraphTech"),
    path('dashboard/demograficos',dashboard ,name="demograficos"),
    path('dashboard/tecnologicos',tecnologics ,name="tecnologicos"),
    path('getQuiz',getQuiz ,name="getQuiz"),
    path('createQuizz/',getAnswers, name="createQuizz"),
    path('getAnswers/',getAnswers2, name="getAnswers"),
    path('getSurvey',getSurveyStatus, name="getSurvey"),
    path('getOnline/<str:code>',getOnline, name="getOnline"),    
    path('getQuestions/<str:code>',getQuestions, name="getQuestion"),    
    path('getRegister/<str:code>',getRegister, name="getRegisters"),    
    
]

