from django.forms import *

from core.models import *

from datetime import datetime

from django.shortcuts import render


class QuestionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['question'].widget.attrs['autofocus'] = True

    class Meta:
        model = Question
        fields = '__all__'
        widgets = {
            'question': Textarea(
                attrs={
                    'placeholder': 'Pregunta al ponente',
                    #'rows': 5,
                    #'cols': 40,
                }
            ),
            'user': TextInput(
                attrs={
                    'placeholder': 'Usuario',
                }
            ),
            'email': TextInput(
                attrs={
                    'placeholder': 'Email',
                }
            ),
            'code': TextInput(
                attrs={
                    'placeholder': 'Code',
                }
            ),
        }

class TrviaForm(ModelForm):

    class Meta:
            model = Trivia
            fields = '__all__'
            widgets = {
                'question': TextInput(
                    attrs={
                        'placeholder': 'Question',
                    }
                ),
                'answer': TextInput(
                    attrs={
                        'placeholder': 'Answer',
                    }
                ),

                'user': TextInput(
                    attrs={
                        'placeholder': 'Usuario',
                    }
                ),
                'mailUser': TextInput(
                    attrs={
                        'placeholder': 'Email',
                    }
                ),
                'score': TextInput(
                    attrs={
                        'placeholder': 'Score',
                    }
                )               
            }

class SurveyForm(ModelForm):

    class Meta:
            model = Survey
            fields = '__all__'
            widgets = {
                'user': TextInput(
                    attrs={
                        'placeholder': 'User',
                    }
                ),
                'email': EmailInput(
                    attrs={
                        'placeholder': 'Email',
                    }
                ),
                's1': TextInput(
                    attrs={
                        'placeholder': 'Survey 1',
                    }
                ),
                's2': TextInput(
                    attrs={
                        'placeholder': 'Survey 2',
                    }
                ),
                's3': TextInput(
                    attrs={
                        'placeholder': 'Survey 3',
                    }
                ),
            }

class SpeakerForm(ModelForm):

    class Meta:
            model = Speaker
            fields = '__all__'
            widgets = {
                'name': TextInput(
                    attrs={
                        'placeholder': 'Nombre',
                    }
                ),
                'job': TextInput(
                    attrs={
                        'placeholder': 'Puesto',
                    }
                ),
                'description': Textarea(
                    attrs={
                        'placeholder': 'Email',
                    }
                ),
            }
