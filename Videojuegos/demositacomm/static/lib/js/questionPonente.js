
    function listError(obj)
     {
         $.each(obj, function (key, value)
         {
            //console.log(key); //tipo de error
            console.log(value[0]); //Valor del error
            alertQuestion();
            alertQuestion("danger","fa fa-exclamation-triangle",value[0]);   

         });
     }
    
     function clearQuestion()
     {
        $('textarea[name="question"]').val("");  
        alertQuestion("success","fa fa-check-circle","Tu pregunta ha sido enviada ");   
     }

     function alertQuestion(tipe,icon,msg) {
        $('#alert_placeholderQuestion').html('<div class="alert alert-'+tipe+'"><button type="button" class="close" data-dismiss="alert">&times;</button><i class="fa fa-exclamation-triangle"></i><strong>  ' + msg + '&nbsp;&nbsp; </strong></div>');
     }