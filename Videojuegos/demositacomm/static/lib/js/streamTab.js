var panelChat=document.getElementById("panelChat");
var panelQuestion=document.getElementById("panelQuestion");
var panelQuizz=document.getElementById("panelQuizz");
var panelSurvey=document.getElementById("panelSurvey");

var contChat=document.getElementById("chatStr");
var contQuestion=document.getElementById("questions");
var contQuizz=document.getElementById("quizz");
var contSurvey=document.getElementById("survey");


function viewChat()
{
  panelChat.classList.add("active");
  panelQuestion.classList.remove("active");
  panelQuizz.classList.remove("active");
  panelSurvey.classList.remove("active");

  contChat.classList.add("active");
  contQuestion.classList.remove("active");
  contQuizz.classList.remove("active");
  contSurvey.classList.remove("active");
  getChatStr();
  console.log("1");
}

function viewQuestion()
{
  panelQuestion.classList.add("active");
  panelChat.classList.remove("active");
  panelQuizz.classList.remove("active");
  panelSurvey.classList.remove("active");

  contQuestion.classList.add("active");
  contChat.classList.remove("active");
  contQuizz.classList.remove("active");
  contSurvey.classList.remove("active");
  console.log("2");
}

function viewQuizz()
{
  panelQuizz.classList.add("active");
  panelChat.classList.remove("active");
  panelQuestion.classList.remove("active");
  panelSurvey.classList.remove("active");

  contQuizz.classList.add("active");
  contChat.classList.remove("active");
  contQuestion.classList.remove("active");
  contSurvey.classList.remove("active");
  console.log("3");
  initQuizz();

}

function viewSurvey()
{
  panelSurvey.classList.add("active");
  panelChat.classList.remove("active");
  panelQuestion.classList.remove("active");
  panelQuizz.classList.remove("active");

  contSurvey.classList.add("active");
  contChat.classList.remove("active");
  contQuestion.classList.remove("active");
  contQuizz.classList.remove("active");
  console.log("4");
  activateSurvey();
}
